import alsaaudio
import sys
import hashlib

# Configuration for your soundcard
device="hw:CARD=CODEC,DEV=0"

rate = 48000
channels = 1
period = 1
format = alsaaudio.PCM_FORMAT_S16_LE

record = alsaaudio.PCM(type=alsaaudio.PCM_CAPTURE, rate=rate, channels=channels, format=format, periodsize=period, device=device)

# Recording and output
values = []
while 1:
    sample = record.read()[1]

    if len(sample) > 0:
        values += [sample[i] for i in range(len(sample))]
        while len(values)/64 >= 1:
            sys.stdout.buffer.write(hashlib.sha512(bytearray([values.pop() for i in range(64)])).digest())
