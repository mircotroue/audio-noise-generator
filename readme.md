# Audio Noise True Random Number Generator

## Working principle
The script uses the python package alsaaudio to read white noise from a white noise generator connected to a sound card.

You can use `arecord -L` to find the correct one and use the name in the script.
The settings have to be adapted to get the highest output rates.

For balancing of the distribution, the random input audio byte stream is used to calculate a SHA-512 hash with exactly 64 bytes of input. The hash is then written to stdout.

The Generation of 1MB data takes roughly 11.3s on an Intel(R) Core(TM) i5-5300U CPU @ 2.30GHz when using mono audio with a sampling rate of 48kHz in 16-bit.
For comparison: The bitrate of the audio is 48000 kHz * 16 bit / 8 bit/byte = 96 kB/s. (1MB would take 10.4s)

## The Noise Source
The SG-10 white noise source by EQKIT was used. The schematic can be seen below. The frequency curve was acquired in Audacity with a one minute lone audio sample using a FFT.

![audio source](schematic.png)
![spectrum](spectrum.png)

## Example Data
Without noise source:
```
b'\x01\x00\xff\xff\x01\x00\xff\xff\xff\xff\xff\xff\x00\x00\x00\x00\xff\xff\x00\x00\x00\x00\xff\xff\xff\xff\x00\x00\xfe\xff\xff\xff\xfe\xff\x01\x00\xfe\xff\x00\x00\xff\xff\xff\xff\xfe\xff\x00\x00\xff\xff\x00\x00\xff\xff\x00\x00\xff\xff\xff\xff\x00\x00\xff\xff\xff\xff\x00\x00\xff\xff\xff\xff\xff\xff\x01\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\x01\x00\x00\x00\xff\xff\xff\xff\xff\xff'
```
With noise source:
```
b' (\xe52\xc0\x1aY,?(q\x1d\x92(\x88\x10\x1c\x04\x06\x14*\x1a9\x02s\x13\x03\n\xab\xf2\xee\xee,\x06\xe2\x17.\x16\xa9\xfd\xa2\xf8\x0e\xe8\xfc\xdb\n\xe74\xd8H\xdfe\xe4\xe3\xc8\x9f\xcd\xe9\xe6\xc9%\x06/\xfc((\x13w6\xc6+\xe5\x10\x88\n\x97\xfe\x14\xf6\xcd\xdc\x86\xeai\xd4\xce\xe2\xf1\xd9\xc4\xe1\xeb\xe2j\xf2'
```

## Testing
The testing file contained 1 GB of random data. The first 1k bytes are printed in random-example.txt. A test file can be created with `python3 generate.py | head -c 1G > random.txt`
### Entropy Test
```
Entropy = 8.000000 bits per byte.

Optimum compression would reduce the size
of this 1073741824 byte file by 0 percent.

Chi square distribution for 1073741824 samples is 275.64, and randomly
would exceed this value 17.88 percent of the times.

Arithmetic mean value of data bytes is 127.4978 (127.5 = random).
Monte Carlo value for Pi is 3.141560901 (error 0.00 percent).
Serial correlation coefficient is -0.000003 (totally uncorrelated = 0.0).
```
### FIBS 140-2 Test
```
rngtest 5
Copyright (c) 2004 by Henrique de Moraes Holschuh
This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

rngtest: starting FIPS tests...
rngtest: entropy source drained
rngtest: bits received from input: 8589934592
rngtest: FIPS 140-2 successes: 429147
rngtest: FIPS 140-2 failures: 349
rngtest: FIPS 140-2(2001-10-10) Monobit: 48
rngtest: FIPS 140-2(2001-10-10) Poker: 49
rngtest: FIPS 140-2(2001-10-10) Runs: 120
rngtest: FIPS 140-2(2001-10-10) Long run: 135
rngtest: FIPS 140-2(2001-10-10) Continuous run: 0
rngtest: input channel speed: (min=924.600; avg=2002578.663; max=19531250.000)Kibits/s
rngtest: FIPS tests speed: (min=716.533; avg=136504.168; max=171326.754)Kibits/s
rngtest: Program run time: 65769311 microseconds
```
### Dieharder Test
```
#=============================================================================#
#            dieharder version 3.31.1 Copyright 2003 Robert G. Brown          #
#=============================================================================#
   rng_name    |           filename             |rands/second|
        mt19937|                      random.txt|  8.67e+07  |
#=============================================================================#
        test_name   |ntup| tsamples |psamples|  p-value |Assessment
#=============================================================================#
   diehard_birthdays|   0|       100|     100|0.52047015|  PASSED  
      diehard_operm5|   0|   1000000|     100|0.16144826|  PASSED  
  diehard_rank_32x32|   0|     40000|     100|0.85999466|  PASSED  
    diehard_rank_6x8|   0|    100000|     100|0.74972438|  PASSED  
   diehard_bitstream|   0|   2097152|     100|0.50140326|  PASSED  
        diehard_opso|   0|   2097152|     100|0.76093222|  PASSED  
        diehard_oqso|   0|   2097152|     100|0.85593500|  PASSED  
         diehard_dna|   0|   2097152|     100|0.08170188|  PASSED  
diehard_count_1s_str|   0|    256000|     100|0.73125089|  PASSED  
diehard_count_1s_byt|   0|    256000|     100|0.31029236|  PASSED  
 diehard_parking_lot|   0|     12000|     100|0.33188578|  PASSED  
    diehard_2dsphere|   2|      8000|     100|0.08935264|  PASSED  
    diehard_3dsphere|   3|      4000|     100|0.09359798|  PASSED  
     diehard_squeeze|   0|    100000|     100|0.90712590|  PASSED  
        diehard_sums|   0|       100|     100|0.15489765|  PASSED  
        diehard_runs|   0|    100000|     100|0.85228703|  PASSED  
        diehard_runs|   0|    100000|     100|0.43322377|  PASSED  
       diehard_craps|   0|    200000|     100|0.43149352|  PASSED  
       diehard_craps|   0|    200000|     100|0.82073752|  PASSED  
 marsaglia_tsang_gcd|   0|  10000000|     100|0.26528057|  PASSED  
 marsaglia_tsang_gcd|   0|  10000000|     100|0.35954816|  PASSED  
         sts_monobit|   1|    100000|     100|0.53912737|  PASSED  
            sts_runs|   2|    100000|     100|0.62372961|  PASSED  
          sts_serial|   1|    100000|     100|0.32405189|  PASSED  
          sts_serial|   2|    100000|     100|0.33157337|  PASSED  
          sts_serial|   3|    100000|     100|0.65466700|  PASSED  
          sts_serial|   3|    100000|     100|0.05108665|  PASSED  
          sts_serial|   4|    100000|     100|0.69748303|  PASSED  
          sts_serial|   4|    100000|     100|0.83943491|  PASSED  
          sts_serial|   5|    100000|     100|0.80526472|  PASSED  
          sts_serial|   5|    100000|     100|0.79029114|  PASSED  
          sts_serial|   6|    100000|     100|0.15108331|  PASSED  
          sts_serial|   6|    100000|     100|0.06601942|  PASSED  
          sts_serial|   7|    100000|     100|0.90567190|  PASSED  
          sts_serial|   7|    100000|     100|0.56101255|  PASSED  
          sts_serial|   8|    100000|     100|0.92420153|  PASSED  
          sts_serial|   8|    100000|     100|0.69718767|  PASSED  
          sts_serial|   9|    100000|     100|0.18861277|  PASSED  
          sts_serial|   9|    100000|     100|0.97668829|  PASSED  
          sts_serial|  10|    100000|     100|0.11297247|  PASSED  
          sts_serial|  10|    100000|     100|0.36941856|  PASSED  
          sts_serial|  11|    100000|     100|0.16547754|  PASSED  
          sts_serial|  11|    100000|     100|0.54537938|  PASSED  
          sts_serial|  12|    100000|     100|0.12991262|  PASSED  
          sts_serial|  12|    100000|     100|0.58507222|  PASSED  
          sts_serial|  13|    100000|     100|0.26603927|  PASSED  
          sts_serial|  13|    100000|     100|0.95397825|  PASSED  
          sts_serial|  14|    100000|     100|0.76562701|  PASSED  
          sts_serial|  14|    100000|     100|0.39147672|  PASSED  
          sts_serial|  15|    100000|     100|0.36033696|  PASSED  
          sts_serial|  15|    100000|     100|0.25091208|  PASSED  
          sts_serial|  16|    100000|     100|0.35659546|  PASSED  
          sts_serial|  16|    100000|     100|0.27106529|  PASSED  
         rgb_bitdist|   1|    100000|     100|0.35263582|  PASSED  
         rgb_bitdist|   2|    100000|     100|0.70339811|  PASSED  
         rgb_bitdist|   3|    100000|     100|0.77052360|  PASSED  
         rgb_bitdist|   4|    100000|     100|0.99998422|   WEAK   
         rgb_bitdist|   5|    100000|     100|0.38065170|  PASSED  
         rgb_bitdist|   6|    100000|     100|0.92491905|  PASSED  
         rgb_bitdist|   7|    100000|     100|0.28495572|  PASSED  
         rgb_bitdist|   8|    100000|     100|0.75258756|  PASSED  
         rgb_bitdist|   9|    100000|     100|0.33769908|  PASSED  
         rgb_bitdist|  10|    100000|     100|0.04046379|  PASSED  
         rgb_bitdist|  11|    100000|     100|0.84013859|  PASSED  
         rgb_bitdist|  12|    100000|     100|0.97221925|  PASSED  
rgb_minimum_distance|   2|     10000|    1000|0.58345893|  PASSED  
rgb_minimum_distance|   3|     10000|    1000|0.21773575|  PASSED  
rgb_minimum_distance|   4|     10000|    1000|0.35116732|  PASSED  
rgb_minimum_distance|   5|     10000|    1000|0.14769474|  PASSED  
    rgb_permutations|   2|    100000|     100|0.93791795|  PASSED  
    rgb_permutations|   3|    100000|     100|0.92978417|  PASSED  
    rgb_permutations|   4|    100000|     100|0.09020893|  PASSED  
    rgb_permutations|   5|    100000|     100|0.99454823|  PASSED  
      rgb_lagged_sum|   0|   1000000|     100|0.49749291|  PASSED  
      rgb_lagged_sum|   1|   1000000|     100|0.58693854|  PASSED  
      rgb_lagged_sum|   2|   1000000|     100|0.80728318|  PASSED  
      rgb_lagged_sum|   3|   1000000|     100|0.96676760|  PASSED  
      rgb_lagged_sum|   4|   1000000|     100|0.89916936|  PASSED  
      rgb_lagged_sum|   5|   1000000|     100|0.85744894|  PASSED  
      rgb_lagged_sum|   6|   1000000|     100|0.61303347|  PASSED  
      rgb_lagged_sum|   7|   1000000|     100|0.70006779|  PASSED  
      rgb_lagged_sum|   8|   1000000|     100|0.84035965|  PASSED  
      rgb_lagged_sum|   9|   1000000|     100|0.59800902|  PASSED  
      rgb_lagged_sum|  10|   1000000|     100|0.99357375|  PASSED  
      rgb_lagged_sum|  11|   1000000|     100|0.15326346|  PASSED  
      rgb_lagged_sum|  12|   1000000|     100|0.68263208|  PASSED  
      rgb_lagged_sum|  13|   1000000|     100|0.84984874|  PASSED  
      rgb_lagged_sum|  14|   1000000|     100|0.92180570|  PASSED  
      rgb_lagged_sum|  15|   1000000|     100|0.67609712|  PASSED  
      rgb_lagged_sum|  16|   1000000|     100|0.38916825|  PASSED  
      rgb_lagged_sum|  17|   1000000|     100|0.63874919|  PASSED  
      rgb_lagged_sum|  18|   1000000|     100|0.30486226|  PASSED  
      rgb_lagged_sum|  19|   1000000|     100|0.20525747|  PASSED  
      rgb_lagged_sum|  20|   1000000|     100|0.99151085|  PASSED  
      rgb_lagged_sum|  21|   1000000|     100|0.58535774|  PASSED  
      rgb_lagged_sum|  22|   1000000|     100|0.90677851|  PASSED  
      rgb_lagged_sum|  23|   1000000|     100|0.97709930|  PASSED  
      rgb_lagged_sum|  24|   1000000|     100|0.78311275|  PASSED  
      rgb_lagged_sum|  25|   1000000|     100|0.94366050|  PASSED  
      rgb_lagged_sum|  26|   1000000|     100|0.86280100|  PASSED  
      rgb_lagged_sum|  27|   1000000|     100|0.64275559|  PASSED  
      rgb_lagged_sum|  28|   1000000|     100|0.83402603|  PASSED  
      rgb_lagged_sum|  29|   1000000|     100|0.24938855|  PASSED  
      rgb_lagged_sum|  30|   1000000|     100|0.55801088|  PASSED  
      rgb_lagged_sum|  31|   1000000|     100|0.84233240|  PASSED  
      rgb_lagged_sum|  32|   1000000|     100|0.68261737|  PASSED  
     rgb_kstest_test|   0|     10000|    1000|0.25528808|  PASSED  
     dab_bytedistrib|   0|  51200000|       1|0.29836127|  PASSED  
             dab_dct| 256|     50000|       1|0.07997437|  PASSED  
Preparing to run test 207.  ntuple = 0
        dab_filltree|  32|  15000000|       1|0.64958287|  PASSED  
        dab_filltree|  32|  15000000|       1|0.68823178|  PASSED  
Preparing to run test 208.  ntuple = 0
       dab_filltree2|   0|   5000000|       1|0.77296969|  PASSED  
       dab_filltree2|   1|   5000000|       1|0.78849339|  PASSED  
Preparing to run test 209.  ntuple = 0
        dab_monobit2|  12|  65000000|       1|0.69993404|  PASSED  
```
